# DarkDelivery Backend Prototype

Backend prototype for [DarkDelivery](https://gitlab.com/yaroslav-kulpan/mvp-arena-frontend) project to display minimal
functionality of the app. 

## Getting started
Install requirements:
```bash
# NPM
npm install

# Yarn
yarn install
```

## Using
Just type:
```bash
# NPM
npm start

# Yarn
yarn yarn
```
**By default, server starting on 3000 port.**

## Test working

### Login

Any of the following routes logs an existing user in :

- **`POST /login`**
- **`POST /signin`**

**`email`** and **`password`** are required, of course. Use next to test working server local:

```http
POST /login
{
  "email": "olivier@mail.com",
  "password": "bestPassw0rd"
}
```

[JSON Server documentation](https://github.com/typicode/json-server/blob/master/README.md) |
[JSON Server Auth documentation](https://github.com/jeremyben/json-server-auth/blob/master/README.MD)